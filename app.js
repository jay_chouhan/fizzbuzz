module.exports = {
  fizzbuzz: (num) => {
    if (typeof num === 'number' && num > 0) {
      let numArr = [];
      for (let i = 1; i <= num; i++) {
        if (i % 3 === 0 && i % 5 === 0) {
          numArr.push('fizzbuzz');
        } else if (i % 3 === 0) {
          numArr.push('fizz');
        } else if (i % 5 === 0) {
          numArr.push('buzz');
        } else {
          numArr.push(i);
        }
      }
      return numArr;
    } else {
      throw new Error('Invalid input');
    }
  },
};
