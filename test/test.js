const chai = require('chai');
const chaiThings = require('chai-things');
const { fizzbuzz } = require('../app');

const { should, expect, assert } = chai;
chai.use(chaiThings);

const num = 20;
const fizzbuzzResult = fizzbuzz(num);

describe('fizbuzz', () => {
  it('should be an array', () => {
    expect(fizzbuzzResult).to.be.an('array');
  });

  it('should have the same length as the given number', () => {
    expect(fizzbuzzResult).to.have.lengthOf(num);
  });

  it('should have the string "fizz" when a number is divisible by three', () => {
    expect(
      fizzbuzzResult.filter(
        (num) => (fizzbuzzResult.indexOf(num) + 1) % 3 === 0 && (fizzbuzzResult.indexOf(num) + 1) % 5 !== 0
      )
    ).all.to.be.equal('fizz');
  });

  it('should have the string "buzz" when a number is divisible by five', () => {
    expect(
      fizzbuzzResult.filter(
        (num) => (fizzbuzzResult.indexOf(num) + 1) % 5 === 0 && (fizzbuzzResult.indexOf(num) + 1) % 3 !== 0
      )
    ).all.to.be.equal('buzz');
  });

  it('should have the string "fizzbuzz" when a number is divisible by both three and five', () => {
    expect(
      fizzbuzzResult.filter(
        (num) => (fizzbuzzResult.indexOf(num) + 1) % 5 === 0 && (fizzbuzzResult.indexOf(num) + 1) % 3 === 0
      )
    ).all.to.be.equal('fizzbuzz');
  });
});

describe('errors', () => {
  it(' should throw error when num is not of type number', () => {
    expect(() => {
      fizzbuzz('not a number');
    }).to.throw(Error);
  });

  it('should throw error when num is less than 1', () => {
    expect(() => {
      fizzbuzz(-1);
    }).to.throw(Error);
  });
});
